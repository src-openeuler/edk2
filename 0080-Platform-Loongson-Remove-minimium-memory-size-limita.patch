From ff113b5d376dd9054aefc8452e7ff04459489a44 Mon Sep 17 00:00:00 2001
From: Bibo Mao <maobibo@loongson.cn>
Date: Mon, 25 Mar 2024 10:50:28 +0800
Subject: [PATCH 1/2] Platform/Loongson: Remove minimium memory size limitation

Temparory stack memory on PEI is hardcoded now, also minimium memory
size 256M is hardcoded now. Here memory map table from fw cfg can be
parsed. If there is memory map entry contains pei stack, it can be
published as usable memory at PEI stage.

Cc: Bibo Mao <maobibo@loongson.cn>
Cc: Chao Li <lichao@loongson.cn>
Signed-off-by: Bibo Mao <maobibo@loongson.cn>
Signed-off-by: Xianglai Li <lixianglai@loongson.cn>
Reviewed-by: Chao Li <lichao@loongson.cn>
---
 .../Loongson/LoongArchQemuPkg/Loongson.dec    |  2 -
 .../Loongson/LoongArchQemuPkg/Loongson.dsc    |  6 ---
 .../LoongArchQemuPkg/PlatformPei/MemDetect.c  | 38 ++++++++++++++++++-
 .../PlatformPei/PlatformPei.inf               |  2 -
 4 files changed, 37 insertions(+), 11 deletions(-)

diff --git a/Platform/Loongson/LoongArchQemuPkg/Loongson.dec b/Platform/Loongson/LoongArchQemuPkg/Loongson.dec
index e638b835e4..c2c6cc9596 100644
--- a/Platform/Loongson/LoongArchQemuPkg/Loongson.dec
+++ b/Platform/Loongson/LoongArchQemuPkg/Loongson.dec
@@ -48,8 +48,6 @@
 
   gLoongArchQemuPkgTokenSpaceGuid.PcdSecPeiTempRamBase|0|UINT64|0x0000000b
   gLoongArchQemuPkgTokenSpaceGuid.PcdSecPeiTempRamSize|0|UINT32|0x0000000c
-  gLoongArchQemuPkgTokenSpaceGuid.PcdUefiRamTop|0x0|UINT64|0x0000000d
-  gLoongArchQemuPkgTokenSpaceGuid.PcdRamRegionsBottom|0x0|UINT64|0x0000000e
   gLoongArchQemuPkgTokenSpaceGuid.PcdFlashSecFvBase|0x0|UINT64|0x0000000f
   gLoongArchQemuPkgTokenSpaceGuid.PcdFlashSecFvSize|0x0|UINT32|0x00000010
 
diff --git a/Platform/Loongson/LoongArchQemuPkg/Loongson.dsc b/Platform/Loongson/LoongArchQemuPkg/Loongson.dsc
index 58aa16d3a9..aab2ca9b28 100644
--- a/Platform/Loongson/LoongArchQemuPkg/Loongson.dsc
+++ b/Platform/Loongson/LoongArchQemuPkg/Loongson.dsc
@@ -356,12 +356,6 @@
   gLoongArchQemuPkgTokenSpaceGuid.PcdSecPeiTempRamBase                 | 0x10000
   gLoongArchQemuPkgTokenSpaceGuid.PcdSecPeiTempRamSize                 | 0x10000
   gLoongArchQemuPkgTokenSpaceGuid.PcdDeviceTreeBase                    | 0x100000
-  #
-  # minimal memory for uefi bios should be 512M
-  # 0x00000000 - 0x10000000
-  # 0x90000000 - 0xA0000000
-  #
-  gLoongArchQemuPkgTokenSpaceGuid.PcdUefiRamTop                        | 0x10000000
   gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiExposedTableVersions           | 0x06
 
   gEfiMdeModulePkgTokenSpaceGuid.PcdBootManagerMenuFile                | { 0x21, 0xaa, 0x2c, 0x46, 0x14, 0x76, 0x03, 0x45, 0x83, 0x6e, 0x8a, 0xb6, 0xf4, 0x66, 0x23, 0x31 }
diff --git a/Platform/Loongson/LoongArchQemuPkg/PlatformPei/MemDetect.c b/Platform/Loongson/LoongArchQemuPkg/PlatformPei/MemDetect.c
index 7e6a4a3aa9..7aa6fdc175 100644
--- a/Platform/Loongson/LoongArchQemuPkg/PlatformPei/MemDetect.c
+++ b/Platform/Loongson/LoongArchQemuPkg/PlatformPei/MemDetect.c
@@ -40,12 +40,48 @@ PublishPeiMemory (
   UINT64 Base;
   UINT64      Size;
   UINT64      RamTop;
+  FIRMWARE_CONFIG_ITEM FwCfgItem;
+  UINTN                FwCfgSize;
+  UINTN                Processed;
+  LOONGARCH_MEMMAP_ENTRY  MemoryMapEntry;
 
   //
   // Determine the range of memory to use during PEI
   //
   Base = PcdGet64 (PcdSecPeiTempRamBase) + PcdGet32 (PcdSecPeiTempRamSize);
-  RamTop = PcdGet64 (PcdUefiRamTop);
+  RamTop = 0;
+
+  Status = QemuFwCfgFindFile ("etc/memmap", &FwCfgItem, &FwCfgSize);
+  if (EFI_ERROR (Status)) {
+    return Status;
+  }
+
+  if (FwCfgSize % sizeof MemoryMapEntry != 0) {
+    return EFI_PROTOCOL_ERROR;
+  }
+
+  QemuFwCfgSelectItem (FwCfgItem);
+  for (Processed = 0; Processed < FwCfgSize; Processed += sizeof MemoryMapEntry) {
+    QemuFwCfgReadBytes (sizeof MemoryMapEntry, &MemoryMapEntry);
+    if (MemoryMapEntry.Type != EfiAcpiAddressRangeMemory) {
+      continue;
+    }
+
+    /*
+     * Find memory map entry where PEI temp stack is located
+     */
+    if ((MemoryMapEntry.BaseAddr <= Base) &&
+        (Base < (MemoryMapEntry.BaseAddr + MemoryMapEntry.Length))) {
+        RamTop = MemoryMapEntry.BaseAddr + MemoryMapEntry.Length;
+        break;
+    }
+  }
+
+  if (RamTop == 0) {
+    DEBUG ((DEBUG_ERROR, "ERROR: No memory map entry contains temp stack \n"));
+    ASSERT (FALSE);
+  }
+
   Size = RamTop - Base;
 
   //
diff --git a/Platform/Loongson/LoongArchQemuPkg/PlatformPei/PlatformPei.inf b/Platform/Loongson/LoongArchQemuPkg/PlatformPei/PlatformPei.inf
index 6cc3513b63..65591a4d7b 100644
--- a/Platform/Loongson/LoongArchQemuPkg/PlatformPei/PlatformPei.inf
+++ b/Platform/Loongson/LoongArchQemuPkg/PlatformPei/PlatformPei.inf
@@ -64,8 +64,6 @@
 [FixedPcd]
   gLoongArchQemuPkgTokenSpaceGuid.PcdFlashDxeFvBase
   gLoongArchQemuPkgTokenSpaceGuid.PcdFlashDxeFvSize
-  gLoongArchQemuPkgTokenSpaceGuid.PcdRamRegionsBottom
-  gLoongArchQemuPkgTokenSpaceGuid.PcdUefiRamTop
   gLoongArchQemuPkgTokenSpaceGuid.PcdSecPeiTempRamBase
   gLoongArchQemuPkgTokenSpaceGuid.PcdSecPeiTempRamSize
   gEmbeddedTokenSpaceGuid.PcdPrePiCpuMemorySize
-- 
2.46.0

