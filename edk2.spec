%global stable_date 202308
%global release_tag edk2-stable%{stable_date}
%global openssl_commitid de90e54bbe82e5be4fb9608b6f5c308bb837d355
%global brotli_commitid f4153a09f87cbb9c826d8fc12c74642bb2d879ea 
%global public_mipi_sys_t_commitid 370b5944c046bab043dd8b133727b2135af7747a
%global _python_bytecompile_extra 0

Name: edk2
Version: %{stable_date}
Release: 19
Summary: EFI Development Kit II
License: BSD-2-Clause-Patent and OpenSSL and MIT 
URL: https://github.com/tianocore/edk2
Source0: https://github.com/tianocore/edk2/archive/%{release_tag}.tar.gz
Source1: openssl.%{openssl_commitid}.tar.gz
Source2: brotli.%{brotli_commitid}.tar.gz
Source3: public-mipi-sys-t.%{public_mipi_sys_t_commitid}.tar.gz
Source4: edk2-aarch64-verbose-raw.json
Source5: edk2-ovmf-x64-nosb.json
Source6: edk2-loongarch64.json
# git archive from https://github.com/tianocore/edk2-platforms/tree/35bca3ca71c004b7f3d93c6f33724796c6b1bf0b
Source7: edk2-platforms-35bca3ca71c0.tar.xz

patch0: 0001-OvmfPkg-VirtioNetDxe-Extend-the-RxBufferSize-to-avoi.patch
patch1: 0002-add-Wno-maybe-uninitialized-to-fix-build-error.patch
patch2: 0003-Add-testcases-for-empty-associated-data-entries-with.patch
patch3: 0004-Do-not-ignore-empty-associated-data-with-AES-SIV-mod.patch
patch4: 0005-Add-a-test-for-CVE-2023-3446.patch
patch5: 0006-Fix-DH_check-excessive-time-with-over-sized-modulus.patch
patch6: 0007-Make-DH_check-set-some-error-bits-in-recently-added-.patch
patch7: 0008-DH_check-Do-not-try-checking-q-properties-if-it-is-o.patch
patch8: 0009-dhtest.c-Add-test-of-DH_check-with-q-p-1.patch
patch9: 0010-Add-NULL-checks-where-ContentInfo-data-can-be-NULL.patch
patch10: 0011-poly1305-ppc.pl-Fix-vector-register-clobbering.patch
patch11: 0012-SecurityPkg-DxeTpm2MeasureBootLib-SECURITY-PATCH-411.patch
patch12: 0013-SecurityPkg-DxeTpmMeasureBootLib-SECURITY-PATCH-4117.patch
patch13: 0014-SecurityPkg-Adding-CVE-2022-36763-to-SecurityFixes.y.patch
patch14: 0015-SecurityPkg-DxeTpm2MeasureBootLib-SECURITY-PATCH-411.patch
patch15: 0016-SecurityPkg-DxeTpmMeasureBootLib-SECURITY-PATCH-4118.patch
patch16: 0017-SecurityPkg-Adding-CVE-2022-36764-to-SecurityFixes.y.patch
patch17: 0018-SecurityPkg-DxeTpm2MeasureBootLib-SECURITY-PATCH-411.patch
patch18: 0019-SecurityPkg-DxeTpmMeasureBootLib-SECURITY-PATCH-4117.patch
patch19: 0020-EmbeddedPkg-Hob-Integer-Overflow-in-CreateHob.patch
patch20: 0021-StandaloneMmPkg-Hob-Integer-Overflow-in-CreateHob.patch

# Fix CVE-2023-45229、CVE-2023-45230、CVE-2023-45231、CVE-2023-45232、CVE-2023-45233、CVE-2023-45234、CVE-2023-45235
patch21: 0021-NetworkPkg-Dhcp6Dxe-SECURITY-PATCH-CVE-2023-45230-Pa.patch
patch22: 0022-NetworkPkg-Add-Unit-tests-to-CI-and-create-Host-Test.patch
patch23: 0023-NetworkPkg-Dhcp6Dxe-SECURITY-PATCH-CVE-2023-45230-Un.patch
patch24: 0024-NetworkPkg-Dhcp6Dxe-SECURITY-PATCH-CVE-2023-45229-Pa.patch
patch25: 0025-NetworkPkg-Dhcp6Dxe-SECURITY-PATCH-CVE-2023-45229-Un.patch
patch26: 0026-NetworkPkg-Ip6Dxe-SECURITY-PATCH-CVE-2023-45231-Patc.patch
patch27: 0027-NetworkPkg-Ip6Dxe-SECURITY-PATCH-CVE-2023-45231-Unit.patch
patch28: 0028-NetworkPkg-Ip6Dxe-SECURITY-PATCH-CVE-2023-45232-Patc.patch
patch29: 0029-NetworkPkg-Ip6Dxe-SECURITY-PATCH-CVE-2023-45232-Unit.patch
patch30: 0030-NetworkPkg-UefiPxeBcDxe-SECURITY-PATCH-CVE-2023-4523.patch
patch31: 0031-NetworkPkg-UefiPxeBcDxe-SECURITY-PATCH-CVE-2023-4523.patch
patch32: 0032-MdePkg-Test-Add-gRT_GetTime-Google-Test-Mock.patch
patch33: 0033-NetworkPkg-UefiPxeBcDxe-SECURITY-PATCH-CVE-2023-4523.patch
patch34: 0034-NetworkPkg-UefiPxeBcDxe-SECURITY-PATCH-CVE-2023-4523.patch
patch35: 0035-NetworkPkg-Adds-a-SecurityFix.yaml-file.patch
patch36: 0036-NetworkPkg-Dhcp6Dxe-SECURITY-PATCH-CVE-2023-45229-Re.patch
patch37: 0037-NetworkPkg-Dhcp6Dxe-Removes-duplicate-check-and-repl.patch
patch38: 0038-NetworkPkg-Dhcp6Dxe-Packet-Length-is-not-updated-bef.patch
patch39: 0039-NetworkPkg-Updating-SecurityFixes.yaml.patch

# Fix CVE-2023-6237、CVE-2024-2511
patch40: 0040-Add-a-test-for-session-cache-handling.patch
patch41: 0041-Extend-the-multi_resume-test-for-simultaneous-resump.patch
patch42: 0042-Fix-unconstrained-session-cache-growth-in-TLSv1.3.patch
patch43: 0043-Hardening-around-not_resumable-sessions.patch
patch44: 0044-Add-a-test-for-session-cache-overflow.patch
patch45: 0045-Limit-the-execution-time-of-RSA-public-key-check.patch

# Fix CVE-2024-1298
patch46: 0046-MdeModulePkg-Potential-UINT32-overflow-in-S3-ResumeC.patch

# Fix CVE-2024-5535
patch47: 0047-Fix-SSL_select_next_proto-and-add-ALPN-validation-in.patch
patch48: 0048-Add-a-test-for-ALPN-and-NPN.patch

# Fix CVE-2024-6119
patch49: 0049-Avoid-type-errors-in-EAI-related-name-check-logic.patch

# revert these patches for edk2-platforms to build with edk2-2308
patch50: 0050-Revert-Platform-Loongson-LoongArchQemuPkg-Add-ImageP.patch
patch51: 0051-Revert-Platform-Loongson-Fix-compile-error.patch
patch52: 0052-Revert-LoongArchQemuPkg-auto-gen-fix-SEC-ProcessLibr.patch
# Fix edk2 build errror with gcc-14
patch53: 0053-relax_edk2_gcc14.patch

# Fix CVE-2024-38796
patch54: 0054-MdePkg-Fix-overflow-issue-in-BasePeCoffLib.patch

# Support Hygon CSV3
patch55: 0055-MdePkg-Add-StandardSignatureIsHygonGenuine-in-BaseCp.patch
patch56: 0056-UefiCpuPkg-LocalApicLib-Exclude-second-SendIpi-seque.patch
patch57: 0057-OvmfPkg-Add-CSV-secure-call-library-on-Hygon-CPU.patch
patch58: 0058-OvmfPkg-ResetVector-Support-CSV-in-ResetVector-phase.patch
patch59: 0059-OvmfPkg-PlatformPei-Initialize-CSV-VM-s-memory.patch
patch60: 0060-OvmfPkg-BaseMemcryptSevLib-update-page-status-to-Sec.patch
patch61: 0061-OvmfPkg-Tcg-Add-CsvLib-for-TpmMmioSevDecryptPei.patch
patch62: 0062-OvmfPkg-Add-CsvDxe-driver.patch
patch63: 0063-OvmfPkg-IoMmuDxe-Add-CsvIoMmu-protocol.patch
patch64: 0064-OvmfPkg-Reserve-a-CPUID-table-page-for-CSV-guest.patch
patch65: 0065-OvmfPkg-Use-classic-mmio-window-for-CSV-guest.patch
# Fix CVE-2023-45236、CVE-2023-45237
patch66: 0066-NetworkPkg-TcpDxe-SECURITY-PATCH-CVE-2023-45236-Rela.patch
patch67: 0067-NetworkPkg-DxeNetLib-SECURITY-PATCH-CVE-2023-45237-R.patch

# Support live migrate Hygon CSV/CSV2/CSV3 guest
patch68: 0068-OvmfPkg-BaseMemEncryptLib-Detect-SEV-live-migration-.patch
patch69: 0069-OvmfPkg-BaseMemEncryptLib-Hypercall-API-for-page-enc.patch
patch70: 0070-OvmfPkg-BaseMemEncryptLib-Invoke-page-encryption-sta.patch
patch71: 0071-OvmfPkg-VmgExitLib-Encryption-state-change-hypercall.patch
patch72: 0072-OvmfPkg-PlatformPei-Mark-SEC-GHCB-page-as-unencrypte.patch
patch73: 0073-OvmfPkg-AmdSevDxe-Add-support-for-SEV-live-migration.patch
patch74: 0074-OvmfPkg-BaseMemcryptSevLib-Correct-the-calculation-o.patch
patch75: 0075-OvmfPkg-BaseMemEncryptLib-Return-SUCCESS-if-not-supp.patch
# Fix nesting #VC in mmio check
patch76: 0076-OvmfPkg-BaseMemEncryptLib-Save-memory-encrypt-status.patch

# Support vdpa blk/scsi device boot
patch77: 0077-VirtioDxe-add-support-of-MMIO-Bar-for-virtio-devices.patch
patch78: 0078-Virtio-wait-virtio-device-reset-done.patch
patch79: 0079-VirtioBlk-split-large-IO-according-to-segment_size_m.patch

# Support Loongarch firmware size
patch80: 0080-Platform-Loongson-Remove-minimium-memory-size-limita.patch
patch81: 0081-Platform-Loongson-Modify-loongarch-uefi-firmware-siz.patch
patch82: 0082-fixup-fdt-parse-error.patch

# Fix CVE-2024-13176、CVE-2024-4741、CVE-2023-5363
patch83: 0083-Fix-timing-side-channel-CVE-2024-13176.patch
patch84: 0084-Free-the-read-buffers-CVE-2024-4741.patch
patch85: 0085-Process-key-length-CVE-2023-5363.patch

BuildRequires: acpica-tools gcc gcc-c++ libuuid-devel python3 bc nasm python3-unversioned-command isl 

%description
EDK II is a modern, feature-rich, cross-platform firmware development environment for the UEFI and PI specifications. 

%package devel
Summary: EFI Development Kit II Tools
%description devel
This package provides tools that are needed to build EFI executables and ROMs using the GNU tools.

%package -n python3-%{name}-devel
Summary: EFI Development Kit II Tools
Requires: python3
BuildArch: noarch
%description -n python3-%{name}-devel
This package provides tools that are needed to build EFI executables and ROMs using the GNU tools.

%package help
Summary: Documentation for EFI Development Kit II Tools
BuildArch: noarch
%description help
This package documents the tools that are needed to build EFI executables and ROMs using the GNU tools.

%ifarch aarch64
%package aarch64
Summary: AARCH64 Virtual Machine Firmware
BuildArch: noarch
%description aarch64
EFI Development Kit II AARCH64 UEFI Firmware
%endif

%ifarch x86_64
%package ovmf
Summary: Open Virtual Machine Firmware
BuildArch: noarch
%description ovmf
EFI Development Kit II Open Virtual Machine Firmware (x64)
%endif

%ifarch %{ix86}
%package ovmf-ia32
Summary: Open Virtual Machine Firmware
BuildArch: noarch
%description ovmf-ia32
EFI Development Kit II Open Virtual Machine Firmware (ia32)
%endif

%ifarch riscv64
%package ovmf-riscv64
Summary: Open Virtual Machine Firmware
BuildArch: noarch
%description ovmf-riscv64
EFI Development Kit II Open Virtual Machine Firmware (riscv64)
%endif

%ifarch loongarch64
%package ovmf-loongarch64
Summary: Open Virtual Machine Firmware
BuildArch: noarch
%description ovmf-loongarch64
EFI Development Kit II Open Virtual Machine Firmware (loongarch64)
%endif

%prep
%setup -n edk2-%{release_tag}
tar -xf %{SOURCE1} -C CryptoPkg/Library/OpensslLib/openssl --strip-components=1
tar -xf %{SOURCE2} -C MdeModulePkg/Library/BrotliCustomDecompressLib/brotli --strip-components=1
tar -xf %{SOURCE2} -C BaseTools/Source/C/BrotliCompress/brotli --strip-components=1
tar -xf %{SOURCE3} -C MdePkg/Library/MipiSysTLib/mipisyst --strip-components=1
tar -xf %{SOURCE7} edk2-platforms/Platform --strip-components=1

%autopatch -p1

cp -a -- %{SOURCE4} %{SOURCE5} %{SOURCE6}  .

%build
NCPUS=`/usr/bin/getconf _NPROCESSORS_ONLN`
BUILD_OPTION="-t GCC5 -n $NCPUS -b RELEASE"

make -C BaseTools %{?_smp_mflags} EXTRA_OPTFLAGS="%{optflags}" EXTRA_LDFLAGS="%{__global_ldflags}"
. ./edksetup.sh

COMMON_FLAGS="-D NETWORK_IP6_ENABLE"
%ifarch aarch64
    BUILD_OPTION="$BUILD_OPTION -a AARCH64 -p ArmVirtPkg/ArmVirtQemu.dsc --cmd-len=65536 $COMMON_FLAGS"
    # In order to be compatible with old os, make EFI_LOADER_DATA executable again.
    BUILD_OPTION="$BUILD_OPTION --pcd PcdDxeNxMemoryProtectionPolicy=0xC000000000007FD1"
%endif

%ifarch x86_64
    BUILD_OPTION="$BUILD_OPTION -a X64 -p OvmfPkg/OvmfPkgX64.dsc $COMMON_FLAGS"
%endif

%ifarch %{ix86}
    BUILD_OPTION="$BUILD_OPTION -a IA32 -p OvmfPkg/OvmfPkgIa32.dsc"
%endif
BUILD_OPTION="$BUILD_OPTION -D SECURE_BOOT_ENABLE=TRUE"
BUILD_OPTION="$BUILD_OPTION -D TPM2_ENABLE=TRUE"
BUILD_OPTION="$BUILD_OPTION -D TPM2_CONFIG_ENABLE=TRUE"
BUILD_OPTION="$BUILD_OPTION -D TPM_ENABLE=TRUE"
BUILD_OPTION="$BUILD_OPTION -D TPM_CONFIG_ENABLE=TRUE"

%ifarch riscv64
    BUILD_OPTION="-t GCC5 -n $NCPUS -b RELEASE -a RISCV64 -p OvmfPkg/RiscVVirt/RiscVVirtQemu.dsc -D SECURE_BOOT_ENABLE=TRUE -D TPM_ENABLE=TRUE -D TPM_CONFIG_ENABLE=TRUE"
%endif 

%ifarch loongarch64
    BUILD_OPTION="-t GCC5 -n $NCPUS -b RELEASE -a LOONGARCH64 -p Platform/Loongson/LoongArchQemuPkg/Loongson.dsc -D SECURE_BOOT_ENABLE=TRUE -D TPM_ENABLE=TRUE -D TPM_CONFIG_ENABLE=TRUE"
%endif

build $BUILD_OPTION

%install
cp CryptoPkg/Library/OpensslLib/openssl/LICENSE.txt LICENSE.openssl
mkdir -p %{buildroot}%{_bindir} \
         %{buildroot}%{_datadir}/%{name}/Conf \
         %{buildroot}%{_datadir}/%{name}/Scripts \
         %{buildroot}%{_datadir}/qemu/firmware
install BaseTools/Source/C/bin/* %{buildroot}%{_bindir}
install BaseTools/BuildEnv %{buildroot}%{_datadir}/%{name}
install BaseTools/Conf/*.template %{buildroot}%{_datadir}/%{name}/Conf
install BaseTools/Scripts/GccBase.lds %{buildroot}%{_datadir}/%{name}/Scripts

cp -R BaseTools/Source/Python %{buildroot}%{_datadir}/%{name}/Python
find %{buildroot}%{_datadir}/%{name}/Python -name '__pycache__'|xargs rm -rf 

for i in build BPDG GenDepex GenFds GenPatchPcdTable PatchPcdValue Pkcs7Sign Rsa2048Sha256Sign TargetTool Trim UPT; do
echo '#!/usr/bin/env bash
export PYTHONPATH=%{_datadir}/%{name}/Python${PYTHONPATH:+:"$PYTHONPATH"}
exec python3 '%{_datadir}/%{name}/Python/$i/$i.py' "$@"' > %{buildroot}%{_bindir}/$i
  chmod +x %{buildroot}%{_bindir}/$i
done

echo '#!/usr/bin/env bash
export PYTHONPATH=%{_datadir}/%{name}/Python${PYTHONPATH:+:"$PYTHONPATH"}
exec python3 '%{_datadir}/%{name}/Python/Ecc/EccMain.py' "$@"' > %{buildroot}%{_bindir}/Ecc
chmod +x %{buildroot}%{_bindir}/Ecc

echo '#!/usr/bin/env bash
export PYTHONPATH=%{_datadir}/%{name}/Python${PYTHONPATH:+:"$PYTHONPATH"}
exec python3 '%{_datadir}/%{name}/Python/Capsule/GenerateCapsule.py' "$@"' > %{buildroot}%{_bindir}/GenerateCapsule
chmod +x %{buildroot}%{_bindir}/GenerateCapsule

echo '#!/usr/bin/env bash
export PYTHONPATH=%{_datadir}/%{name}/Python${PYTHONPATH:+:"$PYTHONPATH"}
exec python3 '%{_datadir}/%{name}/Python/Rsa2048Sha256Sign/Rsa2048Sha256GenerateKeys.py' "$@"' > %{buildroot}%{_bindir}/Rsa2048Sha256GenerateKeys
chmod +x %{buildroot}%{_bindir}/Rsa2048Sha256GenerateKeys

%ifarch aarch64
    mkdir -p %{buildroot}/usr/share/%{name}/aarch64
    cp Build/ArmVirtQemu-AARCH64/RELEASE_*/FV/*.fd %{buildroot}/usr/share/%{name}/aarch64
    dd of="%{buildroot}/usr/share/%{name}/aarch64/QEMU_EFI-pflash.raw" if="/dev/zero" bs=1M count=64
    dd of="%{buildroot}/usr/share/%{name}/aarch64/QEMU_EFI-pflash.raw" if="%{buildroot}/usr/share/%{name}/aarch64/QEMU_EFI.fd" conv=notrunc
    dd of="%{buildroot}/usr/share/%{name}/aarch64/vars-template-pflash.raw" if="/dev/zero" bs=1M count=64
    install -m 0644 edk2-aarch64-verbose-raw.json %{buildroot}%{_datadir}/qemu/firmware/10-edk2-aarch64-verbose-raw.json
%endif

%ifarch x86_64
    mkdir -p %{buildroot}/usr/share/%{name}/ovmf
    cp Build/OvmfX64/*/FV/OVMF*.fd %{buildroot}/usr/share/%{name}/ovmf
    install -m 0644 edk2-ovmf-x64-nosb.json %{buildroot}%{_datadir}/qemu/firmware/10-edk2-ovmf-x64-nosb.json
%endif

%ifarch %{ix86}
    mkdir -p %{buildroot}/usr/share/%{name}/ovmf-ia32
    cp Build/OvmfIa32/*/FV/OVMF_CODE.fd %{buildroot}/usr/share/%{name}/ovmf-ia32
%endif

%ifarch riscv64
    mkdir -p %{buildroot}/usr/share/%{name}/ovmf-riscv64
    cp Build/RiscVVirtQemu/RELEASE_GCC5/FV/RISCV_VIRT_CODE.fd %{buildroot}/usr/share/%{name}/ovmf-riscv64
    cp Build/RiscVVirtQemu/RELEASE_GCC5/FV/RISCV_VIRT_VARS.fd %{buildroot}/usr/share/%{name}/ovmf-riscv64
%endif

%ifarch loongarch64
    mkdir -p %{buildroot}/usr/share/%{name}/loongarch64
    cp Build/LoongArchQemu/RELEASE_*/FV/*.fd %{buildroot}/usr/share/%{name}/loongarch64
    dd of="%{buildroot}/usr/share/%{name}/loongarch64/QEMU_EFI-silent-pflash.raw" if="/dev/zero" bs=1M count=4
    dd of="%{buildroot}/usr/share/%{name}/loongarch64/QEMU_EFI-silent-pflash.raw" if="%{buildroot}/usr/share/%{name}/loongarch64/QEMU_EFI.fd" conv=notrunc
    dd of="%{buildroot}/usr/share/%{name}/loongarch64/vars-template-pflash.raw" if="/dev/zero" bs=1M count=16
    install -m 0644 edk2-loongarch64.json %{buildroot}%{_datadir}/qemu/firmware/50-edk2-loongarch64.json
%endif

%files devel
%license License.txt
%license License-History.txt 
%license LICENSE.openssl
%{_bindir}/BrotliCompress
%{_bindir}/DevicePath
%{_bindir}/EfiRom
%{_bindir}/GenCrc32
%{_bindir}/GenFfs
%{_bindir}/GenFv
%{_bindir}/GenFw
%{_bindir}/GenSec
%{_bindir}/LzmaCompress
%{_bindir}/TianoCompress
%{_bindir}/VfrCompile
%{_bindir}/VolInfo
%{_datadir}/%{name}/BuildEnv
%{_datadir}/%{name}/Conf
%{_datadir}/%{name}/Scripts

%files -n python3-%{name}-devel
%{_bindir}/BPDG
%{_bindir}/Ecc
%{_bindir}/GenDepex
%{_bindir}/GenFds
%{_bindir}/GenPatchPcdTable
%{_bindir}/GenerateCapsule
%{_bindir}/Pkcs7Sign
%{_bindir}/PatchPcdValue
%{_bindir}/Rsa2048Sha256GenerateKeys
%{_bindir}/Rsa2048Sha256Sign
%{_bindir}/TargetTool
%{_bindir}/Trim
%{_bindir}/UPT
%{_bindir}/build
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/Python

%files help
%doc BaseTools/UserManuals/*.rtf

%ifarch aarch64
%files aarch64
%license OvmfPkg/License.txt
%license LICENSE.openssl
%dir /usr/share/%{name}
%dir /usr/share/%{name}/aarch64
/usr/share/%{name}/aarch64/QEMU*.fd
/usr/share/%{name}/aarch64/*.raw
%{_datadir}/qemu/firmware/10-edk2-aarch64-verbose-raw.json
%endif

%ifarch x86_64
%files ovmf
%license OvmfPkg/License.txt
%license LICENSE.openssl
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/ovmf
%{_datadir}/qemu/firmware/10-edk2-ovmf-x64-nosb.json
%endif

%ifarch %{ix86}
%license OvmfPkg/License.txt
%license LICENSE.openssl
%files ovfm-ia32
%dir /usr/share/%{name}
%endif

%ifarch riscv64
%license OvmfPkg/License.txt
%license LICENSE.openssl
%files ovmf-riscv64
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/ovmf-riscv64
%endif

%ifarch loongarch64
%license OvmfPkg/License.txt
%license LICENSE.openssl
%files ovmf-loongarch64
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/loongarch64
%{_datadir}/qemu/firmware/50-edk2-loongarch64.json
%endif

%changelog
* Sun Mar 9 2025 shenyage<shenyage1@huawei.com> - 202308-19
- fix bugs for CVE-2023-45236、CVE-2023-45237

* Sun Feb 23 2025 huyu<huyu70@h-partners.com> - 202308-18
- fix CVE-2024-13176、CVE-2024-4741、CVE-2023-5363

* Tue Dec 17 2024 Xiaotian Wu <wuxiaotian@loongson.cn> - 202308-17
- Update LoongArch virtual machine
- 0080-Platform-Loongson-Remove-minimium-memory-size-limita.patch
- 0081-Platform-Loongson-Modify-loongarch-uefi-firmware-siz.patch
- 0082-fixup-fdt-parse-error.patch

* Fri Nov 29 2024 adttil<2429917001@qq.com> - 202308-16
- vdpa: support vdpa blk/scsi device boot

* Wed Oct 23 2024 hanliyang<hanliyang@hygon.cn> - 202308-15
- Add support for live migration of Hygon CSV1/2/3 guests, fix nesting #VC

* Mon Oct 14 2024 shenyage<shenyage1@huawei.com> - 202308-14
- fix CVE-2023-45236、CVE-2023-45237

* Mon Sep 23 2024 hanliyang<hanliyang@hygon.cn> - 202308-13
- Add support for running in Hygon CSV3 guest

* Wed Oct 09 2024 zhangxianting <zhangxianting@uniontech.com> - 202308-12
- fix CVE-2024-38796

* Fri Sep 13 2024 Xiaotian Wu <wuxiaotian@loongson.cn> - 202308-11
- add LoongArch support
- backport edk2-platform to build with edk2-2308

* Mon Sep 9 2024 shenyage<shenyage1@huawei.com> - 202308-10
- fix CVE-2024-6119

* Wed Aug 14 2024 Jiabo Feng<fengjiabo1@huawei.com> - 202308-9
- make EFI_LOADER_DATA executable again.

* Thu Jul 11 2024 shenyage<shenyage1@huawei.com> - 202308-8
- fix CVE-2024-5535

* Tue Jun 11 2024 shenyage<shenyage1@huawei.com> - 202308-7
- fix CVE-2024-1298

* Mon Apr 15 2024 shenyage<shenyage1@huawei.com> - 202308-6
- fix CVE-2023-6237、CVE-2024-2511

* Thu Mar 7 2024 yexiao<yexiao7@huawei.com> - 202308-5
- fix CVE-2023-45229、CVE-2023-45230、CVE-2023-45231、CVE-2023-45232、CVE-2023-45233、CVE-2023-45234、CVE-2023-45235

* Tue Mar 5 2024 yexiao<yexiao7@huawei.com> - 202308-4
- fix CVE-2022-36763、CVE-2022-36764、CVE-2022-36765

* Sat Feb 24 2024 yexiao <yexiao7@huawei.com> - 202308-3
- Fix CVE-2023-3446、CVE-2023-3817、CVE-2024-0727、CVE-2023-2975、CVE-2023-6129

* Thu Jan 25 2024 duyiwei <duyiwei@kylinos.cn> - 202308-2
- Added firmware scanning directory mapping for libvirt XML

* Mon Aug 28 2023 wangliu<wangliu@iscas.ac.cn> - 202308-1
- update edk2 to stable202308

* Thu Jul 13 2023 Jiabo Feng<fengjiabo1@huawei.com> - 202011-13
- solving the compilation failure problem of gcc 12.3.0

* Fri Mar 10 2023 yexiao<yexiao7@huawei.com> - 202011-12
- fix CVE-2022-4304

* Sun Feb 26 2023 chenhuiying<chenhuiying4@huawei.com> - 202011-11
- fix CVE-2023-0286

* Sun Feb 26 2023 chenhuiying<chenhuiying4@huawei.com> - 202011-10
- fix CVE-2023-0215

* Sat Feb 25 2023 shaodenghui<shaodenghui@huawei.com> - 202011-9
- fix CVE-2023-0401

* Mon Feb 20 2023 shaodenghui<shaodenghui@huawei.com> - 202011-8
- fix CVE-2022-4450

* Tue Nov 29 2022 chenhuiying<chenhuiying4@huawei.com> - 202011-7
- fix CVE-2021-38578

* Thu Sep 29 2022 chenhuiying<chenhuiying4@huawei.com> - 202011-6
* fix CVE-2019-11098

* Tue Jun 14 2022 miaoyubo <miaoyubo@huawei.com> - 202011-5
- Enable TPM for pcr0-7

* Wed Apr 27 2022 yezengruan <yezengruan@huawei.com> - 202011-4
- update the format of changelog

* Thu Feb 17 2022 Jinhua Cao <caojinhua1@huawei.com> - 202011-3
- OvmfPkg: VirtioNetDxe: Extend the RxBufferSize to avoid data truncation

* Tue Feb 15 2022 Jinhua Cao <caojinhua1@huawei.com> - 202011-2
- fix CVE-2021-38576

* Mon Feb 7 2022 Jinhua Cao <caojinhua1@huawei.com> - 202011-1
- update edk2 to stable 202011

* Wed Jan 12 2022 Jinhua Cao <caojinhua1@huawei.com> - 202002-11
- BaseTools: fix ucs-2 lookup on python3.9
- BaseTools: Work around array.array.tostring() removal in python3.9

* Wed Dec 1 2021 Jinhua Cao <caojinhua1@huawei.com> - 202002-10
- fix CVE-2021-28216

* Wed Sep 22 2021 imxcc <xingchaochao@huawei.com> - 202002-9
- fix cve-2021-38575

* Tue Aug 31 2021 miaoyubo <miaoyubo@huawei.com> - 202002-8
- MdeModulePkg/LzmaCustomDecompressLib: catch 4GB+ uncompressed

* Fri Jul 30 2021 Zhenyu Ye <yezhenyu2@huawei.com> - 202002-7
- ArmPkg/CompilerIntrinsicsLib: provide atomics intrinsics

* Mon Jun 28 2021 Jiajie Li <lijiajie11@huawei.com> - 202002-6
- Fix CVE-2021-28210

* Tue Oct 27 2020 AlexChen <alex.chen@huawei.com> - 202002-5
- remove build requires of python2

* Mon Sep 28 2020 FangYing <fangying1@huawei.com> - 202002-4
- update the Source0 to http url

* Fri Jul 31 2020 jiangfangjie <jiangfangjie@huawei.com> - 202002-3
- ArmVirtPkg/ArmVirtQemu: enable TPM2 based measured boot
- ArmVirtPkg/ArmVirtQemu: enable the TPM2 configuration module

* Mon Jul 27 2020 zhangxinhao <zhangxinhao1@huawei.com> - 202002-2
- add build option "-D SECURE_BOOT_ENABLE=TRUE" to enable secure boot

* Thu May 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 202002-1
- Update edk2 to stable202002 and OpenSSL to 1.1.1f

* Thu Mar 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 201908-9
- fix an overflow bug in rsaz_512_sqr
- use the correct maximum indent

* Tue Mar 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 201908-8
- enable multiple threads compiling
- Pass EXTRA_OPTFLAGS and EXTRA_OPTFLAGS options to make command
- enable IPv6 for X86_64

* Sun Mar 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 201908-7
- fix missing OVMF.fd in package

* Sat Feb 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 201908-6
- add build requires of python2

* Mon Dec 30 2019 Heyi Guo <buildteam@openeuler.org> - 201908-5
- Upgrade openssl to 1.1.1d

* Tue Nov 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 201908-4
- add build requires of nasm

* Tue Nov 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 201908-3
- Correct name of package ovmf

* Mon Sep 30 2019  zhanghailiang <zhang.zhanghailiang@huawei.com> - 201908-2
- Enable IPv6 suppport and Modify Release number to 2

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 201908-1
- Package init

